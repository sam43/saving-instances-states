package com.sam43.hinextdot;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class NextFragment extends Fragment {
    Unbinder unbinder;

    public NextFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_next, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btn_previous, R.id.btn_nxt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_previous:
                //Back to previous fragment
                FragmentManager fragmentManager = getFragmentManager();
                SateInstanceFragment fragment = new SateInstanceFragment();
                fragmentManager.beginTransaction()
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .replace(R.id.fragment_holder, fragment)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.btn_nxt:

                Toast.makeText(getActivity(), "Will go Next fragment", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
