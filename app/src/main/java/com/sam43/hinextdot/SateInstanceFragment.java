package com.sam43.hinextdot;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class SateInstanceFragment extends Fragment {
    Unbinder unbinder;
    @BindView(R.id.et_input_text)
    TextInputEditText etInputText;
    String stateToSave;


    public SateInstanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        Log.d("steps", "I am inside onCreateView");


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_instance, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("steps", "I am inside onCreate");

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.d("steps", "I am inside onSave");


//        stateToSave = MainActivity.textOfEt;
//        outState.putString("saved_state", stateToSave);
//
//        //outState.putString("save_state",stateToSave);
//        Log.d("steps", "I am inside onSave");


    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        Log.d("steps", "I am inside onViewStateRestored");

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if(MainActivity.textOfEt == null) {

            Log.d("steps", "I am inside if NULL");
            MainActivity.textOfEt = etInputText.getText().toString();
        }
        Log.d("steps", "i am inside onActivityCreated");

//        String stateSaved = savedInstanceState.getString("saved_state");
//
        if(MainActivity.textOfEt == null) {
            Toast.makeText(getActivity(),
                    "onRestoreInstanceState:\nNO state saved!",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(),
                    "onRestoreInstanceState:\nsaved state = " + MainActivity.textOfEt,
                    Toast.LENGTH_LONG).show();

            etInputText.setText(MainActivity.textOfEt);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.btn_next)
    public void onViewClicked() {

        MainActivity.textOfEt = etInputText.getText().toString();

        Toast.makeText(getActivity(),
                "onRestoreInstanceState:\nsaved state = " + MainActivity.textOfEt,
                Toast.LENGTH_LONG).show();


//        if(MainActivity.textOfEt == null) {
//
//            Log.d("steps", "I am inside if NULL");
//            MainActivity.textOfEt = etInputText.getText().toString();
//            Toast.makeText(getActivity(),
//                    "onRestoreInstanceState:\nsaved state = " + MainActivity.textOfEt,
//                    Toast.LENGTH_LONG).show();
//        } else if (MainActivity.textOfEt != null) {
//            Toast.makeText(getActivity(),
//                    "onRestoreInstanceState:\nsaved state = not null = " + MainActivity.textOfEt,
//                    Toast.LENGTH_LONG).show();
//        }

        FragmentManager fragmentManager = getFragmentManager();
        NextFragment fragment = new NextFragment();
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.fragment_holder, fragment)
                .addToBackStack(null)
                .commit();

    }
}
